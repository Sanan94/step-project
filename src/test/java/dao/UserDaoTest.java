package dao;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import az.company.booking_project.dao.UserDao;
import az.company.booking_project.entities.User;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;
import org.junit.jupiter.api.Test;

class UserDaoTest extends UserDao {
    @Test
    void findAll() {
        User user= new User("sananichee", "aA112233!");
        create(user);
        ArrayList<User> users= new ArrayList<>();
        users.add(user);
        assertEquals(users,getAll());
    }


    @Test
    void findById() throws IOException, ClassNotFoundException {
        User user= new User("sananichee", "aA112233!");
        create(user);
        assertEquals(Optional.of(user), getById(user.getId()));
    }


    @Test
    void add() {
        User user= new User("sananichee", "aA112233!");
        create(user);
        assertTrue(create(user));
    }

    @Test
    void remove() {
        User user= new User("sananichee", "aA112233!");
        create(user);
        assertTrue(delete(user.getId()));
    }


}