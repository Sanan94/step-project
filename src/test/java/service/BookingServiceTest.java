package service;

import static org.junit.jupiter.api.Assertions.assertTrue;

import az.company.booking_project.entities.Airline;
import az.company.booking_project.entities.ArrivalCity;
import az.company.booking_project.entities.DepartureCity;
import az.company.booking_project.entities.Flight;
import az.company.booking_project.entities.Passenger;
import az.company.booking_project.services.BookingService;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;

class BookingServiceTest extends BookingService {
public static int count=0;

    @Test
    void createBooking() {
        Flight flight2= new Flight(
                ++count,
                Airline.LUFTHANSA,
                Airline.AIR_FRANCE.getCode()+112,
                DepartureCity.getRandomCity(),
                ArrivalCity.AMSTERDAM,
                45,
                LocalDateTime.now(),
                LocalDateTime.now().plusHours(4)
        );
        Passenger passenger2= new Passenger("Sanan","Mikayilov");
        Passenger passenger3= new Passenger("Huseyn","Huseynov");
        List<Passenger> passengers1= new ArrayList<>();
        passengers1.add(passenger2);
        passengers1.add(passenger3);

assertTrue(createBooking(count,passengers1));
    }
}